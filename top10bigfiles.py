#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 21 16:17:56 2018

@author: vijay
"""

import os
import random
import sys
import operator

'''this function just gives the top 10 biggest files in the directory target_dir
only. It does not recursively check the subdirectories'''
def top10indir(target_dir):
    dir_path = os.path.join(target_dir)
    #dirpath = os.path.abspath(sys.argv[0])
    #to take directory directly fromcmd line///like in C
    all_files = []
    for root,dirs,files in os.walk(dir_path):
        for name in files:
            all_files.append(os.path.join(root,name))
        break
    files_and_sizes = ((path,os.path.getsize(path)) for path in all_files)
    sorted_files_with_size = sorted(files_and_sizes, key = operator.itemgetter(1))
    
    sorted_files_with_size.reverse()
    reverse_sorted_files_with_size = list(sorted_files_with_size)
    print("Top 10 biggest files in "+os.path.basename(target_dir)+" folder is")
    for x in reverse_sorted_files_with_size[:10]:
        print("Filename: "+os.path.basename(x[0])+" Size: "+str(x[1]/(1000000))+" MB")

'''function prints top 10 files in the given target_dir and also all of its
subdirectories, if they have more than 10 files'''
def top10inallsubdirs(target_dir):
    for root,dirs,files in os.walk(target_dir):
        if(os.path.isdir(root)):
            #print(root)
            if(len(files)>9):
                top10indir(root)


##Please change the address of the target directory here to work in your system
target_dir = "/home/vijay/Desktop"
top10inallsubdirs(target_dir)
#top10indir(target_dir)



