import os
import subprocess
import shutil

reorg_dir = "/home/vijay/Downloads/test"
exclude = ()
remove_empty_folders = False

# Also write a prog to randomly generate files on Desktop
''' This function the name of the folder in which a file needs to be
put depending on its extension'''
def dir_names(ext):
    extensions = [[["jpeg","jpg","png" ],"Pics" ],
                  [["mp4","avi" ],"Videos" ],
                  [["doc","pdf","txt","xlsx", "ppt", "pptx" ],"Docs" ],
                  [["mp3" ],"Music" ]
            ]
    match = [item[1] for item in extensions if ext.lower() in item[0] ]
    return match[0] if match else "Others"

''' This code looks up all the files in the given directory and 
makes new folders with names defined above(if they dont exist) and puts
the file in there depending on the extension '''
for root, dirs, files in os.walk(reorg_dir ):
    filelist =[]
    for name in files:
        subject = root+"/"+name
        #print(name)
        if name.startswith("."):
            extension = "hidden_files"
        elif not "." in name:
            extension = "no_extension"
        else:
            extension = dir_names(name[name.rfind(".")+1:] )
            #finds last "." and puts the following characters in the extension
        if not extension in exclude:
            new_dir = reorg_dir +"/"+extension
            if not os.path.exists(new_dir):
                os.mkdir(new_dir)
            shutil.move(subject,new_dir+"/"+name)
            #shutil.copy(subject,new_dir+"/"+name)##to copy instead of move
    break
#break prevents it from going on recursively
    #can also use depth in os.walk to efine a depth
    
'''An optional function to delete all the empty folders inside  the given
directory'''           
def cleanup():  ##to remove enpty empty folders
    filelist =[]
    for root,dirs, files in os.walk(reorg_dir):
        for name in files:
            filelist.append(root+"/"+name)
    
    directories = [item[0] for item in os.walk(reorg_dir)]
    for dr in directories:
        matches = [item for item in filelist if dr in item]
        if len(matches) ==0:
            try:
                shutil.rmtree(dr)
            except FileNotFoundError:
                pass
            
if remove_empty_folders == True:
    cleanup()