# Desktop cleaner and file sort

Cleaner.py : Organises file inside a given directory into separate folders depending on the extension
 HOW to run the code : 
 You just need to put in the address of the directory in reorg_dir.
 The names of desired folders and various more extensions can be added in the function dir_names.
  Also,If remove_empty_folders is set to True, it would delete all the empty folders inside  the given
directory.


top10bigfiles.py
Consists of two functions:
1) top10indir : this function just gives the top 10 biggest files in the directory target_dir only. It does not recursively check the subdirectories
2) top10inallsubdirs : this function prints top 10 files in the given target_dir and also all of its subdirectories, if they have more than 10 files
How to run the code:
 You just need to put in the address of the directory in target_dir. 
You can run any of the two functions or both as required